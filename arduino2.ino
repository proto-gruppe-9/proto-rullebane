#include <Adafruit_NeoPixel.h>
#include <Wire.h>

#define NUM_STRIPS_LEFT 5
#define NUM_PIXELS_PER_STRIP_LEFT 12

#define NUM_STRIPS_RIGHT 7
#define NUM_PIXELS_PER_STRIP_RIGHT 2


// Define the pins for each LED strip
int stripPinsLeft[NUM_STRIPS_LEFT] = {13, 12, 11, 10, 9};
int stripPinsRight[NUM_STRIPS_RIGHT] = {8, 7, 6, 5, 4, 3, 2};

Adafruit_NeoPixel stripsLeft[NUM_STRIPS_LEFT];
Adafruit_NeoPixel stripsRight[NUM_STRIPS_RIGHT];

static int currentStripLeft = 0;
static int currentPixelLeft = 0;
static long lastUpdatedLeft = 0;

static int currentStripRight = 0;
static int currentPixelRight = 0;
static long lastUpdatedRight = 0;

static int flagLeft = 0;
static int flagRight = 0;

bool ballHasWon = false;

void setup() {
  Serial.begin(9600);
  //Set up led-strips left
  for (int i = 0; i < NUM_STRIPS_LEFT; i++) {
    stripsLeft[i] = Adafruit_NeoPixel(NUM_PIXELS_PER_STRIP_LEFT, stripPinsLeft[i], NEO_GRB + NEO_KHZ800);
    stripsLeft[i].begin();
    stripsLeft[i].clear();
    stripsLeft[i].show();
  }
  stripsLeft[2].setPixelColor(0, stripsLeft[2].Color(0, 0, 0));
  stripsLeft[2].show();

  
  //Set up led-strips right
  for (int i = 0; i < NUM_STRIPS_RIGHT; i++) {
    stripsRight[i] = Adafruit_NeoPixel(NUM_PIXELS_PER_STRIP_RIGHT, stripPinsRight[i], NEO_GRB + NEO_KHZ800);
    stripsRight[i].begin();
    stripsRight[i].clear();
    stripsRight[i].show();

  }
  stripsRight[2].setPixelColor(0, stripsRight[2].Color(0, 0, 0));
  stripsRight[2].show();
  stripsRight[5].setPixelColor(0, stripsRight[5].Color(0, 0, 0));
  stripsRight[5].show();

  resetModel();
  

  //Set up connection to arduino 1
  Wire.begin(4);                // Join I2C bus with address #4
  Wire.onReceive(receiveEvent); // Register receive event
  
}

void loop() {
  if (flagLeft == 1 && !ballHasWon) {
    handleLEDs(1);
  }
  if (flagRight == 1 && !ballHasWon) {
    handleLEDs(2);
  }
}

//handles incoming data from arduino1
void receiveEvent(int howMany) {
  for (int i = 0; i < howMany; i++) {
    
  //while (Wire.available()) {  // Ensure all data is processed
    int command = Wire.read();  // Read the incoming byte
    //handleLEDs(command);
    if(command == 1) {
      flagLeft = 1;
    }
    if (command == 2) {
      flagRight = 1;
    }
    if (command > 2) {
      ballHasWon = true;
      handleLEDs(command);
    }
    Serial.println("LED command: ");
    Serial.println(command);
  }
}


void handleLEDs(int command) {
  //Start led strips on left
  if (command == 1) {
    ledStripsLeft();
    
  } 
  //Start led strips on right
  if (command == 2) {
    ledStripsRight();
  } 
  //Turn right led-strips red
  if (command == 3) {
    
    rightRed();
    
  } 
  //Turns left led-strips red
  else if (command == 4) {
    leftRed();
  } 
  //Resets the model
  else if (command == 5) {
    Serial.println("Resetter");
    resetModel();
  }
  
  else if (command == 6) {
    gameWon();
  } 
}


void gameWon() {
  //Right yellow
  for(int currentStrip = 0; currentStrip<NUM_STRIPS_LEFT; currentStrip++) {
        for (int i=0; i<NUM_PIXELS_PER_STRIP_LEFT; i++) {
          stripsLeft[currentStrip].setPixelColor(i, stripsLeft[currentStrip].Color(255, 250, 205));
        }
      }
  for(int currentStrip = 0; currentStrip<NUM_STRIPS_LEFT; currentStrip++) {
    stripsLeft[currentStrip].show();
  }

  //Right yellow
  for(int currentStrip = 0; currentStrip<NUM_STRIPS_RIGHT; currentStrip++) {
        for (int i=0; i<NUM_PIXELS_PER_STRIP_RIGHT; i++) {
          stripsRight[currentStrip].setPixelColor(i, stripsRight[currentStrip].Color(255, 250, 205));
        }
      }
  for(int currentStrip = 0; currentStrip<NUM_STRIPS_RIGHT; currentStrip++) {
    stripsRight[currentStrip].show();
  }
}


//Resets model
void resetModel() {
  currentStripLeft = 0;
  currentPixelLeft = 0;
  lastUpdatedLeft = 0;
  flagLeft = 0;

  currentStripRight = 0;
  currentPixelRight = 0;
  lastUpdatedRight = 0;
  flagRight = 0;

  ballHasWon = false;

  //Reset leds left side
  for(int currentStrip = 0; currentStrip<NUM_STRIPS_LEFT; currentStrip++) {
    stripsLeft[currentStrip].clear();
    stripsLeft[currentStrip].show();
  }

  //Reset leds right side
  for(int currentStrip = 0; currentStrip<NUM_STRIPS_RIGHT; currentStrip++) {
    stripsRight[currentStrip].clear();
    stripsRight[currentStrip].show();
  }
  

}


//Turns all led-strips on the left red at the same time
void leftRed() {
  Serial.println("Venstre Rød");
  for(int currentStrip = 0; currentStrip<NUM_STRIPS_LEFT; currentStrip++) {
        for (int i=0; i<NUM_PIXELS_PER_STRIP_LEFT; i++) {
          stripsLeft[currentStrip].setPixelColor(i, stripsLeft[currentStrip].Color(255, 0, 0));
        }
      }
  for(int currentStrip = 0; currentStrip<NUM_STRIPS_LEFT; currentStrip++) {
    stripsLeft[currentStrip].show();
  }

}

  

  
//Turns all led-strips on the right red at the same time
void rightRed() {
  Serial.println("Høyre rød");
  for(int currentStrip = 0; currentStrip<NUM_STRIPS_RIGHT; currentStrip++) {
        for (int i=0; i<NUM_PIXELS_PER_STRIP_RIGHT; i++) {
          stripsRight[currentStrip].setPixelColor(i, stripsRight[currentStrip].Color(255, 0, 0));
        }
      }
  for(int currentStrip = 0; currentStrip<NUM_STRIPS_RIGHT; currentStrip++) {
    stripsRight[currentStrip].show();
  }
}



//Starts running the led-strips on the left parallell with the ball
void ledStripsLeft() {  
  	long currentTime = millis();
  	int interval = 66;

  	//Updates next pixel each interval
  	if(currentTime-lastUpdatedLeft >= interval) {
        //Resetting all pixels on the current led-strip
        if(currentPixelLeft == 0) {
          for(int j = 0; j<NUM_PIXELS_PER_STRIP_LEFT; j++) {
            stripsLeft[currentStripLeft].setPixelColor(j, stripsLeft[currentStripLeft].Color(0, 0, 0));
          }
          stripsLeft[currentStripLeft].show();
        }
        //Sets one pixel at the time
        stripsLeft[currentStripLeft].setPixelColor(currentPixelLeft, stripsLeft[currentStripLeft].Color(0, 0, 255));
        stripsLeft[currentStripLeft].show();
      
        lastUpdatedLeft = currentTime;
      
      currentPixelLeft ++;
      
  
      //Moves to next led-strip
      if (currentPixelLeft >= NUM_PIXELS_PER_STRIP_LEFT) {
        currentPixelLeft = 0;
        currentStripLeft++;
        

      }
  
  	}
  
}



//Starts running the led-strips on the right parallell with the ball
void ledStripsRight() {
  	long currentTime = millis();
  	int interval = 140;

    //Updates next pixel each interval
  	if(currentTime-lastUpdatedRight >= interval) {
      //Resetting all pixels on the current led-strip
      if(currentPixelRight == 0) {
         for(int j = 0; j<NUM_PIXELS_PER_STRIP_RIGHT; j++) {
         	stripsRight[currentStripRight].setPixelColor(j, stripsRight[currentStripRight].Color(0, 0, 0));
         }
         stripsRight[currentStripRight].show();
      }

      //Sets one pixel at the time
      stripsRight[currentStripRight].setPixelColor(currentPixelRight, stripsRight[currentStripRight].Color(0, 255, 0));
      stripsRight[currentStripRight].show();
      

      lastUpdatedRight = currentTime;

      currentPixelRight ++;
      
  
      //Move to next led-strip
      if (currentPixelRight >= NUM_PIXELS_PER_STRIP_RIGHT) {
        currentPixelRight = 0;
        currentStripRight++;
        if(currentStripRight >= NUM_STRIPS_RIGHT)
          currentStripRight = NUM_STRIPS_RIGHT-1;

      }
  
  	}
  
  
}

