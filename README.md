# Proto Rullebane
###

## Files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.stud.idi.ntnu.no/proto-gruppe-9/proto-rullebane.git
git branch -M main
git push -uf origin main
```

## Description
To kuler i hver sin kulebane der det er om å gjøre å time utløsningen av kulene slik at de faller i bakken samtidig. Modellen restarter seg selv automatisk etter 8 sekunder og kulene settes tilbake på plass med en magnet av bruker. Kan spilles sammen eller alene.

## Authors and acknowledgment
#### Group 9
Ine Andrea Krogseth, Dorthe Martinussen, Jakob Nermoen og Solvor Kristine Omli-Moe.

## Libraries
New Ping (v1.9.7):  
Created by Tim Eckel - teckel@leethost.com
00003 // Copyright 2012 License: GNU GPL v3 http://www.gnu.org/licenses/gpl-3.0.html

Adafruit Neopixel (v1.12.2):
 https://github.com/adafruit/Adafruit_NeoPixel.git
