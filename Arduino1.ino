#include <Adafruit_NeoPixel.h>
#include <NewPing.h>
#include <Wire.h>
#include <Servo.h>
#include <avr/wdt.h>

// Ultrasonic sensor configuration
#define TRIGGER_PIN_LEFT 6
#define ECHO_PIN_LEFT 5
#define MAX_DISTANCE 3 // Maximum distance for both sensors in cm
#define PING_INTERVAL 1000 // Ping interval in milliseconds

#define TRIGGER_PIN_RIGHT 12
#define ECHO_PIN_RIGHT 11

NewPing sonar_LEFT(TRIGGER_PIN_LEFT, ECHO_PIN_LEFT, MAX_DISTANCE);
NewPing sonar_RIGHT(TRIGGER_PIN_RIGHT, ECHO_PIN_RIGHT, MAX_DISTANCE);

unsigned long previousPingTime_LEFT = 0;
unsigned long previousPingTime_RIGHT = 0;
float distance_LEFT;
float distance_RIGHT;

// LED strip configuration
#define NUM_PIXELS_AROUND 58
#define PIN_NEO_PIXEL_AROUND 13
Adafruit_NeoPixel NeoPixel(NUM_PIXELS_AROUND, PIN_NEO_PIXEL_AROUND, NEO_GRB + NEO_KHZ800);

// Timing and animation settings
#define UPDATE_INTERVAL 200
#define UPDATE_INTERVAL_LEFT 150
#define UPDATE_INTERVAL_RIGHT 95

//defining button pins
int leftButtonPin = 8;
int rightButtonPin = 7;
int resetButtonPin = 9;

//Keeping track of updating led-strip
unsigned long lastUpdateLeftLed = 0;
unsigned long lastUpdateRightLed = 0;
int currentLedIndexLeft = 0;
int currentLedIndexRight = NUM_PIXELS_AROUND - 1;

//Sets which ball reached the bottom first 
bool leftSideWon = false;
bool rightSideWon = false;

//Sets if the led-strip is active
bool leftLedActive = false;
bool rightLedActive = false;

//TRENGS DETTE?
unsigned long startTimer = 0; // Global to keep track across loop iterations
unsigned long leftBallStartTime;
unsigned long rightBallStartTime;
bool leftBallDropped = false;
bool rightBallDropped = false;
float calculatedRunTimeLeft = 5.0; // Example time in seconds
float calculatedRunTimeRight = 2.0; // Example time in seconds

long leftBallEndTime;
long rightBallEndTime;

//Sets state of button 
bool leftButtonPushed = false;
bool rightButtonPushed = false;
bool rightButtonHasBeenPressed = false;
bool leftButtonHasBeenPressed = false;

//Checks if the led-strip is turned off
bool ledAroundCleared = false;

//Sets when the first ball reached the bottom
long gameEndedTime = 0;
static bool gameEnded = false;

// Servo LEFT
Servo servoLeft;
const int servoPinLeft = 4;   // PWM pin connected to the servo
const long rotationTime360Left = 1000;  // Time required to rotate 360 degrees in milliseconds (adjust as needed)

long currentMillisServoLeft;
unsigned long lastPressTimeLeft = 0;  // Store the last press time
unsigned long rotationStartTimeLeft = 0;  // Store the rotation start time
bool isRotatingLeft = false;  // Track if the servo is currently rotating

// Servo RIGHT
Servo servoRight;
const int servoPinRight = 3;
const long rotationTime360Right = 1000;  // Time required to rotate 360 degrees in milliseconds (adjust as needed)

unsigned long lastPressTimeRight = 0;  // Store the last press time
unsigned long rotationStartTimeRight = 0;  // Store the rotation start time
unsigned long currentMillisServoRight;
bool isRotatingRight = false;  // Track if the servo is currently rotating


void setup() {
  NeoPixel.begin();
  NeoPixel.clear();
  NeoPixel.show();
  Serial.begin(9600);

  pinMode(leftButtonPin, INPUT_PULLUP);
  pinMode(rightButtonPin, INPUT_PULLUP);
  pinMode(resetButtonPin, INPUT_PULLUP);

  servoLeft.attach(servoPinLeft);
  servoRight.attach(servoPinRight);
  servoLeft.write(0); // Ensure the servo is at right angle
  servoRight.write(180);

  Wire.begin(); // Join I2C bus as master
}

//Function to control the led-strip around the model (rainbow)
void animateRainbow(unsigned long currentMillis) {
    static unsigned long lastUpdate = 0;
    int interval = 1; //rotationspeed
    if (currentMillis - lastUpdate >= interval) {
        for (int i = 0; i < NUM_PIXELS_AROUND; i++) {
            uint16_t hue = (65535L * (i + (currentMillis / 41)) / NUM_PIXELS_AROUND) % 65535;
            NeoPixel.setPixelColor(i, NeoPixel.ColorHSV(hue, 255, 255));
        }
        NeoPixel.show();
        lastUpdate = currentMillis;
    }
}

//Hard restart the model
void restart() {
  wdt_disable();
  wdt_enable(WDTO_15MS);
  while (1) {}
}

//Control the left servo
void leftServo() {
  if (!isRotatingLeft) {
    servoLeft.write(140);  // Start rotating
    rotationStartTimeLeft = currentMillisServoLeft;  // Record the start time of the rotation
    isRotatingLeft = true;  // Set the rotation flag to true
    Serial.println("venstre motor kjører");

  }
  
  if (isRotatingLeft && currentMillisServoLeft - rotationStartTimeLeft >= rotationTime360Left) {
    servoLeft.write(0);  // Stop the servo by setting to neutral position
    isRotatingLeft = false;  // Reset the rotation flag
  }
  
}

//Control the right servo
void rightServo() {
  if (!isRotatingRight) {
    servoRight.write(40);  // Start rotating
    rotationStartTimeRight = currentMillisServoRight;  // Record the start time of the rotation
    isRotatingRight = true;  // Set the rotation flag to true
    Serial.println("høyre motor kjører");
    Serial.println(servoRight.read());

  }

  
  if (isRotatingRight && currentMillisServoRight - rotationStartTimeRight >= rotationTime360Right) {
    servoRight.write(-180);  // Stop the servo by setting to neutral position
    isRotatingRight = false;  // Reset the rotation flag
  }
  
}

//Controls the left side of the led-strip around
void updateLeftLed(long currentMillis) {
  //Light the left side of the led-strip one by one pixel
  if (leftLedActive && millis() - lastUpdateLeftLed >= UPDATE_INTERVAL_LEFT) {
    if (currentLedIndexLeft < NUM_PIXELS_AROUND / 2 && currentLedIndexRight != NUM_PIXELS_AROUND / 2 - 1) {
      NeoPixel.setPixelColor(currentLedIndexLeft, NeoPixel.Color(0, 0, 255));
      NeoPixel.show();
      currentLedIndexLeft++;
      lastUpdateLeftLed = millis();
      if(currentLedIndexLeft == NUM_PIXELS_AROUND / 2 -1) {
        leftSideWon = true;
      }
    } else {
      leftLedActive = false;
      rightLedActive = false;
      gameEnded = true;

      //If the right ball reached the bottom first, light gap red
      if (currentLedIndexLeft != NUM_PIXELS_AROUND / 2 - 1 && currentLedIndexLeft != 0) {
        for (int i = currentLedIndexLeft; i < NUM_PIXELS_AROUND / 2; i++) {
          NeoPixel.setPixelColor(i, NeoPixel.Color(255, 0, 0));
        }
        NeoPixel.show();
      }
      //If the right ball was never pushed, light the gap red
      if(!rightButtonHasBeenPressed) {
        for (int i = currentLedIndexRight; i <= NUM_PIXELS_AROUND / 2+1; i--) {
          NeoPixel.setPixelColor(i, NeoPixel.Color(255, 0, 0));
        }
        NeoPixel.show();
      }
    }
  }
}

//Controls the right side of the led-strip around
void updateRightLed(unsigned long currentMillis) {
  //Light the right side of the led-strip one by one pixel
  if (rightLedActive && millis() - lastUpdateRightLed >= UPDATE_INTERVAL_RIGHT) {
    if (currentLedIndexRight >= NUM_PIXELS_AROUND / 2 && currentLedIndexLeft != NUM_PIXELS_AROUND / 2) {
      NeoPixel.setPixelColor(currentLedIndexRight, NeoPixel.Color(0, 255, 0));
      NeoPixel.show();
      currentLedIndexRight--;
      lastUpdateRightLed = millis();

      if(currentLedIndexRight == NUM_PIXELS_AROUND / 2) {
        rightSideWon = true;
      }
    } else {
      rightLedActive = false;
      leftLedActive = false;
      gameEnded = true;

      //If the left ball reached the bottom first, light gap red
      if (currentLedIndexRight != NUM_PIXELS_AROUND / 2 && currentLedIndexRight != 0) {
        for (int i = currentLedIndexRight; i > NUM_PIXELS_AROUND / 2 - 1; i--) {
          NeoPixel.setPixelColor(i, NeoPixel.Color(255, 0, 0));
        }
        NeoPixel.show();
        
      }
      //If the left ball was never pushed, light the gap red
      if(!leftButtonHasBeenPressed) {
        for (int i = currentLedIndexLeft; i < NUM_PIXELS_AROUND / 2; i++) {
          NeoPixel.setPixelColor(i, NeoPixel.Color(255, 0, 0));
        }
        NeoPixel.show();
      }
    }
  }
}

//Resets the model after the balls has reached the bottom
void resetModel() {
  //Tells arduino2 to reset led-strips
  NeoPixel.clear();
  Wire.beginTransmission(4); // Transmit to device #4
  Wire.write(5);             // Sends byte '1' to indicate LED strip should turn on
  Wire.endTransmission();

  leftLedActive = false;
  rightLedActive = false;
  leftBallDropped = false;
  rightBallDropped = false;

  lastUpdateLeftLed = 0;
  lastUpdateRightLed = 0;
  currentLedIndexLeft = 0;
  currentLedIndexRight = NUM_PIXELS_AROUND - 1;

  leftButtonPushed = false;
  rightButtonPushed = false;
  ledAroundCleared = false;

  leftButtonHasBeenPressed = false;
  rightButtonHasBeenPressed = false;

  leftSideWon = false;
  rightSideWon = false;

  gameEndedTime = 0;

  //Restart servo
  servoLeft.write(0); // Ensure the servo is stopped initially
  servoRight.write(180);
}

//Controlls the model
void manageBallsAndLeds(unsigned long currentMillis) {
  //Reads the buttons state
  int leftButtonPressed = digitalRead(leftButtonPin);
  int rightButtonPressed = digitalRead(rightButtonPin);


  if (leftButtonPressed == 0 ) {
    leftButtonPushed = true;
    leftButtonHasBeenPressed = true;
  } else {
    leftButtonPushed = false;
  }

  if (rightButtonPressed == 0 ) {
    rightButtonPushed = true;
    rightButtonHasBeenPressed = true;
  } else {
    rightButtonPushed = false;
  }

  currentMillisServoLeft = currentMillis;

  //Tells arduino2 to start left led if left button is pushed
  if (leftButtonPushed && !gameEnded && distance_LEFT < MAX_DISTANCE &&distance_LEFT != 0) {
    Wire.beginTransmission(4); // Transmit to device #4
    Wire.write(1);             // Sends byte '1' to indicate LED strip should turn on
    Wire.endTransmission();

    if (!ledAroundCleared) {
      NeoPixel.clear();
      ledAroundCleared = true;
    }
    leftBallDropped = true;
    leftLedActive = true;
    leftButtonPressed = false;
  }

  //Tells arduino2 to start right led if right button is pushed
  if (rightButtonPushed && !gameEnded && distance_RIGHT < MAX_DISTANCE &&distance_RIGHT != 0) {
    currentMillisServoRight = millis();
    Wire.beginTransmission(4); // Transmit to device #4
    Wire.write(2);             // Sends byte '1' to indicate LED strip should turn on
    Wire.endTransmission();

    if (!ledAroundCleared) {
      NeoPixel.clear();
      ledAroundCleared = true;
    }
    rightBallDropped = true;
    rightLedActive = true;
    rightButtonPressed = false;

    if (!leftBallDropped) {
      startTimer = millis();
      rightBallStartTime = 0;
    } else {
      rightBallStartTime = startTimer;
    }

    rightBallEndTime = calculatedRunTimeRight + rightBallStartTime;
  }

  //Starts left side of led-strip and servo if left button is pushed
  if (leftButtonHasBeenPressed ) {
    updateLeftLed(currentMillis);
    leftServo();
  }
  //Starts right side of led-strip and servo if right button is pushed
  if (rightButtonHasBeenPressed ) {
    updateRightLed(currentMillis);
    rightServo();
  }

  
  if(currentLedIndexRight == NUM_PIXELS_AROUND/2 && currentLedIndexLeft == NUM_PIXELS_AROUND/2-1) {
    Wire.beginTransmission(4); // Transmit to device #4
    Wire.write(6);             // Sends byte '1' to indicate LED strip should turn on
    Wire.endTransmission();

    //Yellow light around
    for (int i = 0; i <= NUM_PIXELS_AROUND ; i++) {
      NeoPixel.setPixelColor(i, NeoPixel.Color(255, 250, 205));
    }
    NeoPixel.show();

    //spinningRainbow();
  }
  
  

  //Resets the model 8 seconds after ended game
  if (gameEndedTime != 0 && millis() > gameEndedTime + 8000) {
    resetModel();
    gameEnded = false;
  }

  //Starts rainbow light if game is not active
  if (!leftLedActive && !rightLedActive && !gameEnded) {
    animateRainbow(currentMillis);
  }

  //If left ball reached the bottom first, turn right lights red
  if (leftSideWon && gameEndedTime == 0) {
    gameEndedTime = millis();

    leftButtonPushed = false;
    rightButtonPushed = false;

    //Tell arduino2 to turn right lights red
    Wire.beginTransmission(4); // Transmit to device #4
    Wire.write(3);             // Sends byte '1' to indicate LED strip should turn on
    Wire.endTransmission();

    //If right ball reached the bottom first, turn left lights red
  } else if (rightSideWon && gameEndedTime == 0) {
    gameEndedTime = millis();

    leftButtonPushed = false;
    rightButtonPushed = false;

    //Tell arduino2 to turn left lights red
    Wire.beginTransmission(4); // Transmit to device #4
    Wire.write(4);             // Sends byte '1' to indicate LED strip should turn on
    Wire.endTransmission();
  }
}

/*
void spinningRainbow() {
  static unsigned long lastUpdate = 0;
    static int rotationsCompleted = 0;
    const int rotationSpeed = 50;  // Milliseconds per hue shift
    const int rotationsNeeded = 5;

    if (currentMillis - lastUpdate >= rotationSpeed) {
        for (int i = 0; i < NUM_PIXELS_AROUND; i++) {
            uint16_t hue = (65535L / NUM_PIXELS_AROUND * (i + rotationsCompleted * NUM_PIXELS_AROUND)) % 65535;
            NeoPixel.setPixelColor(i, NeoPixel.ColorHSV(hue, 255, 255));
        }
        NeoPixel.show();
        lastUpdate = currentMillis;
        rotationsCompleted = (rotationsCompleted + 1) % (rotationsNeeded * NUM_PIXELS_AROUND);

        if (rotationsCompleted / NUM_PIXELS_AROUND >= rotationsNeeded) {
            // Reset rotations or stop the animation
            rotationsCompleted = 0;  // Uncomment this to keep repeating
            // return; // Uncomment this to stop after 5 rotations
        }
    }
}
*/


void loop() {
  unsigned long currentMillis = millis();

  // Update sonar distances
  if (currentMillis - previousPingTime_LEFT >= PING_INTERVAL) {
    previousPingTime_LEFT = currentMillis;
    distance_LEFT = sonar_LEFT.ping_cm();
    Serial.print("Distance LEFT: ");
    Serial.println(distance_LEFT);
  }

  if (currentMillis - previousPingTime_RIGHT >= PING_INTERVAL) {
    previousPingTime_RIGHT = currentMillis;
    distance_RIGHT = sonar_RIGHT.ping_cm();
    Serial.print("Distance RIGHT: ");
    Serial.println(distance_RIGHT);
  }

  manageBallsAndLeds(currentMillis);

  //Hard resets model
  if (digitalRead(resetButtonPin) == LOW) {
    Serial.println("resetting...");
    delay(100);
    //Tells arduino2 to reset model
    Wire.beginTransmission(4); // Transmit to device #4
    Wire.write(5);             // Sends byte '1' to indicate LED strip should turn on
    Wire.endTransmission();
    delay(200);
    restart();
  }
}
